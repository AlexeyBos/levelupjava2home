package org.levelup;

import org.hibernate.SessionFactory;
import org.levelup.hibernate.JobSessionFactoryConfiguration;
import org.levelup.job.list.domain.Position;
import org.levelup.job.list.hibernate.PositionHibernateService;

import java.util.Collection;

public class HomeWork3Positions {

    public static void main(String[] args) {
        SessionFactory factory = new JobSessionFactoryConfiguration().configure();
        PositionHibernateService service = new PositionHibernateService(factory);
        Collection<Position> positions = service.findAllPositions();

        positions.forEach(position -> {
            System.out.println(position.getId() + " " + position.getName());
        });

        Position position = service.findPositionByName("QA");
        System.out.println(position.getId() + " " + position.getName());

        position = service.createPosition("Manager");
        System.out.println("New position: " + position.getId() + " " + position.getName());

        service.deletePositionById(position.getId());

        position = service.createPosition("Manager");
        System.out.println("New position: " + position.getId() + " " + position.getName());

        service.deletePositionByName(position.getName());

        positions = service.findAllPositionWhichNameLike("%evelop%");
        positions.forEach(findPosition -> {
            System.out.println(findPosition.getId() + " " + findPosition.getName());
        });
        factory.close();
    }

}
