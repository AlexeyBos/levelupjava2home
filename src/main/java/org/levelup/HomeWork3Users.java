package org.levelup;

import org.hibernate.SessionFactory;
import org.levelup.hibernate.JobSessionFactoryConfiguration;
import org.levelup.job.list.domain.User;
import org.levelup.job.list.hibernate.UserHibernateService;

import java.util.Collection;

public class HomeWork3Users {

    public static void main(String[] args) {
        SessionFactory factory = new JobSessionFactoryConfiguration().configure();
        UserHibernateService service = new UserHibernateService(factory);

        User user = service.findByPassport("1234 123456");
        System.out.println(user.getId() + " " + user.getName() + " " + user.getLast_name() + " " + user.getPassport());

        user = service.createUser("9865 534472", "Dima", "Dim");
        System.out.println(user.getId() + " " + user.getName() + " " + user.getLast_name() + " " + user.getPassport());

        service.deleteUserByPassport("9865 534472");

        Collection<User> users = service.findByNameAndLastName("Leo", "King");
        users.forEach(rowUser -> {
            System.out.println(rowUser.getId() + " " + rowUser.getName() + " " + rowUser.getLast_name() + " " + rowUser.getPassport());
        });

        users = service.findByLastName("King");
        users.forEach(rowUser -> {
            System.out.println(rowUser.getId() + " " + rowUser.getName() + " " + rowUser.getLast_name() + " " + rowUser.getPassport());
        });

        user = service.updateUser("5453 469764", "newName", "newLastName");

        factory.close();
    }

}
