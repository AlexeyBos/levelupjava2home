package org.levelup.lessons;

import org.levelup.reflection.annotation.RandomInt;

public class SecondClass {

    @RandomInt(min = 4, max = 20)
    private int oneField;
    private String twoFiled;
    private Long threeField;

    private SecondClass() {
        this.oneField = 2;
        this.twoFiled = "value2";
        this.threeField = 22L;
    }

    @Override
    public String toString() {
        return "SecondClass{" +
                "oneField=" + oneField +
                ", twoFiled='" + twoFiled + '\'' +
                ", threeField=" + threeField +
                '}';
    }
}
