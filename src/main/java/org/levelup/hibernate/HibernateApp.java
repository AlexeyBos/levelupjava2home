package org.levelup.hibernate;

import org.hibernate.SessionFactory;
import org.levelup.hibernate.service.UserService;

public class HibernateApp {

    public static void main(String[] args) {
        SessionFactory factory = new JobSessionFactoryConfiguration().configure();

        UserService userService = new UserService(factory);
        //User user = userService.createUserPersist("Oleg", "Olegov", "5612 123412");
        //System.out.println(user);

        //Integer id = userService.createUserSave("Dmitry", "Protsko", "6745 897899");
        //System.out.println(id);

        //Integer cloneId = userService.cloneUser(12, "3489 111222");
        //System.out.println(cloneId);

        //User user = userService.updateUserNameWithMerge(13, "Kolia");
        //System.out.println("copied user: " + Integer.toHexString(user.hashCode()));

        //User user = userService.mergeNewUser("Ivan", "Ivan", "9755 768594");
        //System.out.println(user);

        userService.updateUser("Ivan", "Ivan", "9755 768594");
        factory.close();
    }

}
