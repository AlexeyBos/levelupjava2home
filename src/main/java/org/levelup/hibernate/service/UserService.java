package org.levelup.hibernate.service;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.levelup.hibernate.domain.UserHib;

public class UserService {

    private final SessionFactory factory;

    public UserService(SessionFactory factory) {
        this.factory = factory;
    }

    public UserHib createUserPersist(String name, String lastName, String passport) {
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        UserHib userHib = new UserHib();
        userHib.setName(name);
        userHib.setLastName(lastName);
        userHib.setPassport(passport);

        session.persist(userHib);
        transaction.commit();
        session.close();

        return userHib;
    }

    public Integer createUserSave(String name, String lastName, String passport) {
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        UserHib userHib = new UserHib();
        userHib.setName(name);
        userHib.setLastName(lastName);
        userHib.setPassport(passport);

        Integer generatedId = (Integer) session.save(userHib);

        transaction.commit();
        session.close();

        return generatedId;
    }

    public UserHib getById(Integer id) {
        Session session = factory.openSession();
        UserHib userHib = session.get(UserHib.class, id);
        session.close();

        return userHib;
    }

    public Integer cloneUser(Integer id, String passport) {
        UserHib userHib = getById(id);
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        userHib.setPassport(passport);
        Integer cloneId = (Integer) session.save(userHib);

        transaction.commit();
        session.close();

        return cloneId;
    }

    public UserHib updateUserNameWithMerge(Integer id, String name) {
        UserHib userHib = getById(id);
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        userHib.setName(name);
        UserHib mergedUserHib = (UserHib) session.merge(userHib);

        transaction.commit();
        session.close();

        System.out.println("original user: " + Integer.toHexString(userHib.hashCode()));
        return mergedUserHib;
    }

    public UserHib mergeNewUser(String name, String lastName, String passport) {
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        UserHib userHib = new UserHib();
        userHib.setName(name);
        userHib.setLastName(lastName);
        userHib.setPassport(passport);

        UserHib newUserHib = (UserHib) session.merge(userHib);

        transaction.commit();
        session.close();

        return newUserHib;
    }

    public void updateUser(String name, String lastName, String passport) {
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        UserHib userHib = new UserHib();
        userHib.setName(name);
        userHib.setLastName(lastName);
        userHib.setPassport(passport);

        session.update(userHib);

        transaction.commit();
        session.close();

    }
}
