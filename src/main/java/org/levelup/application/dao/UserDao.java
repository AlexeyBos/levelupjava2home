package org.levelup.application.dao;

import org.levelup.hibernate.domain.UserHib;

import java.util.Collection;

public interface UserDao {

    UserHib createUser(String name, String lastName, String passport, Collection<String> addresses);

}
