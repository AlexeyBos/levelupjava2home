package org.levelup.application.dao;

import lombok.SneakyThrows;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.levelup.application.domain.CompanyEntity;

import java.lang.reflect.Method;
import java.util.List;
import java.util.function.Function;

public class CompanyDaoImpl extends AbstractDao implements CompanyDao {

    public CompanyDaoImpl(SessionFactory factory) {
        super(factory);
    }

    @Override
    public void create(String name, String ein, String address) {
        /*Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        CompanyEntity companyEntity = new CompanyEntity();
        companyEntity.setName(name);
        companyEntity.setEin(ein);
        companyEntity.setAddress(address);

        session.persist(companyEntity);

        transaction.commit();
        session.close();*/

        runInTransaction(session -> {
            CompanyEntity companyEntity = new CompanyEntity();
            companyEntity.setName(name);
            companyEntity.setEin(ein);
            companyEntity.setAddress(address);
            session.persist(companyEntity);
        });

    }

    @Override
    public CompanyEntity findById(Integer id) {
        /*Session session = factory.openSession();

        CompanyEntity companyEntity = session.get(CompanyEntity.class, id);

        //CompanyEntity companyEntity = session.load(CompanyEntity.class, id);
        System.out.println("Entity class: " + companyEntity.getClass().getName());

        session.close();
        System.out.println("Entity name: " + companyEntity.getName());*/
        /*performWithoutTransaction(new DataBaseOperation() { //анонимный внутренний класс
            @Override
            public void doAction(Session session) {
                session.get(CompanyEntity.class, id);
            }
        });*/
        //а это лямбда вышеизложенного
        //CompanyEntity entity = performWithoutTransaction(session -> session.get(CompanyEntity.class, id));
        return runWithoutTransaction(session -> session.get(CompanyEntity.class, id));
        //(parameters) -> {code here}
        //session -> session.get(.....)
    }

    @Override
    public CompanyEntity findByEin(String ein) {

        return runWithoutTransaction(session -> {
            return session
                    .createQuery("from CompanyEntity where ein = :ein", CompanyEntity.class)
                    .setParameter("ein", ein)
                    .getSingleResult();
        });
        //getSingleResult() не очень надежный, поэтому лучше использовать getResultList. А здесь оставили для тестов
    }

    @Override
    public CompanyEntity findByName(String name) {
        /*List<CompanyEntity> companies = session.createQuery("from CompanyEntity where name = :name", CompanyEntity.class)
                .setParameter("name", name)
                .getResultList();*/
        List<CompanyEntity> entities = runWithoutTransaction(session -> {
            return session.createQuery("from CompanyEntity where name = :name", CompanyEntity.class)
                    .setParameter("name", name)
                    .getResultList();
        });
        return entities.isEmpty() ? null : entities.get(0);
    }

    @SneakyThrows
    private void performDataBaseOperation(Method method, Object object, Object... args) {
        Session session = factory.openSession();
        method.invoke(object, args);
        session.close();
    }

    //Supplier

    private <TYPE> TYPE perform(Function<Session, TYPE> function) {
        Session session = factory.openSession();
        TYPE result = function.apply(session);
        session.close();
        return result;
    }

    private <TYPE> TYPE performTransaction(Function<Session, TYPE> function) {
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();
        TYPE result = function.apply(session);
        transaction.commit();
        return result;
    }

    private <T> T performWithoutTransaction(DataBaseOperation<T> operation) {
        Session session = factory.openSession();
        //Action, method
        T result = operation.doAction(session);
        session.close();
        return result;
    }

    interface DataBaseOperation<T> {
        T doAction(Session session);
    }

}
