package org.levelup.application;

import org.hibernate.SessionFactory;
import org.levelup.application.dao.CompanyDao;
import org.levelup.application.dao.CompanyDaoImpl;
import org.levelup.application.dao.CompanyLegalDetailsDao;
import org.levelup.application.dao.CompanyLegalDetailsDaoImpl;
import org.levelup.application.dao.UserDao;
import org.levelup.application.dao.UserDaoImpl;
import org.levelup.application.domain.CompanyEntity;
import org.levelup.application.domain.CompanyLegalDetailsEntity;
import org.levelup.application.domain.UserAddressEntity;
import org.levelup.hibernate.JobSessionFactoryConfiguration;
import org.levelup.hibernate.domain.UserHib;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class JobApplication {

    public static void main(String[] args) {

        SessionFactory factory = new JobSessionFactoryConfiguration().configure();
        CompanyDao companyDao = new CompanyDaoImpl(factory);
        CompanyLegalDetailsDao companyLegalDetailsDao = new CompanyLegalDetailsDaoImpl(factory);
        /*companyDao.create("Company 100", "685-6695567", "Address 100");
        CompanyEntity company = companyDao.findByEin("685-6695567");
        companyLegalDetailsDao.updateLegalDetailsInCompany(company.getId(), "Sberbank", "3534546474");

        Collection<CompanyLegalDetailsEntity> legalDetails = companyLegalDetailsDao.findAllByBankName("Sberbank");
        legalDetails.forEach(legalDetail -> System.out.println(legalDetail.getCompany().getName()));*/

        UserDao userDao = new UserDaoImpl(factory);
        UserHib user = userDao.createUser("Alex", "Alexeev", "5555 555555", new ArrayList<>(Arrays.asList(
                "address 1",
                "address 2",
                "address 3"
        )));

        for (UserAddressEntity addressEntity : user.getAddresses()) {
           System.out.println(addressEntity.getId() + " " + addressEntity.getAddress());
        }

        /*companyDao.findById(1);

        CompanyEntity company = companyDao.findByEin("12346456");
        System.out.println(company);

        companyDao.create("Apple", "1029384756", "Cupertino");*/

        factory.close();

    }

}
