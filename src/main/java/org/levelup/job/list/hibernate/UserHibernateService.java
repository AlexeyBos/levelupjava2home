package org.levelup.job.list.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.levelup.job.list.domain.User;
import org.levelup.job.list.impl.users.UserService;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Collection;
import java.util.List;

public class UserHibernateService implements UserService {

    private final SessionFactory factory;

    public UserHibernateService(SessionFactory factory) {
        this.factory = factory;
    }

    @Override
    public User createUser(String passport, String name, String lastName) {
        User user = findByPassport(passport);
        if (user == null) {
            Session session = factory.openSession();
            Transaction transaction = session.beginTransaction();
            user = new User();
            user.setName(name);
            user.setLast_name(lastName);
            user.setPassport(passport);
            session.persist(user);
            transaction.commit();
            session.close();
        } else {
            System.out.println("User с паспортом \"" + passport + "\" уже существует");
        }
        return user;
    }

    @Override
    public User findByPassport(String passport) {
        Session session = factory.openSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<User> cq = cb.createQuery(User.class);
        Root<User> root = cq.from(User.class);
        cq.select(root).where(cb.equal(root.get("passport"), passport));
        Query query = session.createQuery(cq);
        List users = query.getResultList();
        User user = null;
        if (users.size() > 0) {
            user = (User) users.get(0);
        }
        session.close();
        return user;
    }

    @Override
    public Collection<User> findByNameAndLastName(String name, String lastName) {
        Session session = factory.openSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<User> cq = cb.createQuery(User.class);
        Root<User> root = cq.from(User.class);

        cq.select(root).where(cb.equal(root.get("name"), name), cb.equal(root.get("last_name"), lastName));
        Query query = session.createQuery(cq);
        Collection<User> users = query.getResultList();
        session.close();
        return users;
    }

    @Override
    public Collection<User> findByLastName(String lastName) {
        Session session = factory.openSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<User> cq = cb.createQuery(User.class);
        Root<User> root = cq.from(User.class);

        cq.select(root).where(cb.equal(root.get("last_name"), lastName));
        Query query = session.createQuery(cq);
        Collection<User> users = query.getResultList();
        session.close();
        return users;
    }

    @Override
    public void deleteUserByPassport(String passport) {
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();
        User user = findByPassport(passport);
        session.delete(user);
        transaction.commit();
        session.close();
    }

    @Override
    public User updateUser(String passport, String name, String lastName) {
        User user = findByPassport(passport);
        User mergedUser = null;
        if (user != null) {
            Session session = factory.openSession();
            Transaction transaction = session.beginTransaction();
            user.setPassport(passport);
            user.setLast_name(lastName);
            user.setName(name);

            mergedUser = (User) session.merge(user);

            transaction.commit();
            session.close();
        }

        return mergedUser;
    }
}
